﻿module classic.base;

public import helper;

abstract class ClassicBase
{
	/* mixin function to initialize the pool of cards as immutable */
	private static string initPool()
	{
		size_t count = (Value.max + 1) * (Suit.max + 1);
		
		string insert = "[";
		for (auto i = 0; i < count; i++)
			insert ~= "Card(" ~ to!string(i) ~ "),";
		return insert[0..$-1] ~ "]";
	}

	/* card type definitions */
	enum Value { Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King }
	enum Suit  { Clubs, Diamonds, Hearts, Spades }
	@property size_t maxValue() { return Value.max + 1; }
	@property size_t maxSuit() { return Suit.max + 1; }

	/* logic Structs */
	struct Card
	{
		immutable size_t raw;
		@property Value value()  const { return to!Value(raw % (Value.max + 1)); }
		@property Suit suit() const { return to!Suit(raw / (Value.max + 1)); }
		@property string toString() const { return to!string(value) ~ " of " ~ to!string(suit); }

		this(size_t index) { raw = index; }
	}

	struct Stack
	{
		Card*[] card;

		@property Card* getTop() { return card[0]; }
		@property Card* getBottom() { return card[-1]; }
		@property size_t count() { return card.length; }

		void shuffle()
		{
			import std.random : randomShuffle;
			randomShuffle(card);
		}

		static @property ptrdiff_t top() { return -1; }
		static @property ptrdiff_t bottom() { return -2; }

		string view(ptrdiff_t index = 0)
		{
			if (card.length == 0) return "Stack is empty.";

			if (index >= card.length || index == bottom) index = card.length - 1;
			else if (index < 0) index = 0;

			return card[index].toString();
		}

		bool draw(Card** taking, ptrdiff_t index = 0)
		{
			if (card.length == 0) return false;

			if (index >= card.length || index == bottom) index = card.length - 1;
			else if (index < 0) index = 0;
			
			*taking = card[index];

			card = card[0..index] ~ card[index+1..$]; 
			return true;
		}

		void insert(Card* giving, ptrdiff_t index = 0)
		{
			if (card.length == 0) { card = [giving]; return; }

			if (index >= card.length || index == bottom) index = card.length - 1;
			else if (index < 0) index = 0;

			card = card[0..index] ~ [giving] ~ card[index..$];
		}

		Card*[] draw(size_t amount = 1, ptrdiff_t index = 0)
		{
			Card*[] taking;
			Card* pull;

			for (auto i = 0; i < amount; i++)
			{
				if (!draw(&pull, index)) break;
				taking ~= pull;
			}

			return taking;
		}

		void insert(Card*[] giving, ptrdiff_t index = 0)
		{
			if (giving.length == 0) return;
			
			foreach_reverse(c; giving)
				insert(c, index);
		}
	}
	
	/* master deck object */
	immutable(Card[]) pool = mixin(ClassicBase.initPool);
	@property size_t totalCards() { return pool.length; }

	/* universal components */
	Stack deck,
		  discard;
		
	Stack[] player;


	/* overrides */
	abstract @property size_t minPlayers();
	abstract @property size_t maxPlayers();

	abstract void playGame();
	abstract void getInput(size_t index, string input);

	this()
	{
		/* initialize the deck */
		{
			for (auto i = 0; i < pool.length; i++)
				deck.card ~= cast(Card*)&pool[i];
			shuffleDeck();
		}

		/* Init the players */ 
		{
			writeln("How many people are going to play? (", minPlayers ,"-", maxPlayers, ")");

			size_t playerCount = toNum(readLine);
			while (playerCount < minPlayers || playerCount > maxPlayers)
			{
				writeln("Invalid input. Please enter a number between ", minPlayers ," and ", maxPlayers, ".");
				playerCount = toNum(readLine);
			} player.length = playerCount;

			clear();
			writeln("How many cards should each player start with? (1-", (totalCards / maxPlayers), ")");

			size_t drawCount = toNum(readLine);
			while (drawCount < 1 || drawCount > (totalCards / maxPlayers)) {
				if (drawCount < 1) writeln("Error: Must draw at least 1 card!");
				else writeln("Error: Not enough cards in deck to draw [", drawCount, "] per player!");
				drawCount = toNum(readLine);
			} 
			foreach(ref p; player) p.insert(deck.draw(drawCount));
		}

		/* Start actual game */
		{
			clear();
			playGame();
		}
	}

	void drawCard(size_t playerIndex, size_t amount = 1, bool combineDiscardOnEmpty = true)
	{
		Card* swap;
		while (amount > 0)
		{
			if (!deck.draw(&swap))
			{
				if (!combineDiscardOnEmpty || discard.count == 0) return;
				shuffleDeck(true);
			}
			else
			{
				player[playerIndex].insert(swap);
				amount -= 1;
			}
		}
	}

	void shuffleDeck(bool combineDiscard = true)
	{
		if (combineDiscard) deck.insert(discard.draw(discard.count));
		deck.shuffle();
	}

	void refreshPlayerScreen(size_t index) {
		clear();

		writeln("Player ", index+1, "'s turn...");
		writeln("Hand: [", player[index].count, "]");

		for(auto i = 0; i < player[index].count; i++)
			writeln("[",i,"] ", player[index].card[i].toString());

		writeln("\nType '/help' for a list of commands.\n");
	}
}